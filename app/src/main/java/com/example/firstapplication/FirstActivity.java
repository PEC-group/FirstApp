
package com.example.firstapplication;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * This is the first Activity where user gets to enter his name, address and contact no.
 * Also there is a hidden button which is shown when the submit button is clicked.
 * The hidden button takes us to the next Activity.
 *
 * Notice how we have implemented multiple ClickListeners for handling click and long click.
 */
public class FirstActivity extends Activity implements View.OnClickListener, View.OnLongClickListener {

    // initialize variables
    // EditTexts for fetching the user input
    private EditText editTextName, editTextAddress, editTextPhone;
    // Buttons to submit and go to next activity
    private Button buttonSubmit, buttonNextActivity;
    // TextView to show result after clicking submit
    private TextView textViewDisplay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // setting the layout for this activity
        // This is a compulsory step. Every activity should have its layout.
        setContentView(R.layout.activity_first);

        // mapping/getting the EditTexts from the layout(i.e activity_first.xml)
        // every views(EditText, TextView, Buttons etc must be initialized)
        editTextName = (EditText) findViewById(R.id.edittext_name);
        editTextAddress = (EditText) findViewById(R.id.edittext_address);
        editTextPhone = (EditText) findViewById(R.id.edittext_phone);

        // initializing "Submit" Button and setting click listener to it
        buttonSubmit = (Button) findViewById(R.id.button_submit);
        buttonSubmit.setOnClickListener(this);
        buttonSubmit.setOnLongClickListener(this);

        // initializing "Go to next activity" Button and setting click listener to it
        buttonNextActivity = (Button) findViewById(R.id.button_next_activity);
        buttonNextActivity.setOnClickListener(this);

        // initializing TextView for displaying result
        textViewDisplay = (TextView) findViewById(R.id.textview_display);
    }

    @Override
    /**
     * This method is called when a view("Submit" or "Go to next Activity" button in our case) is clicked.
     *
     *@params button the clicked button
     */
    public void onClick(View button) {
        // We are using the same ClickListener for the two buttons
        // so we need to check which button it is.
        // This can be done by checking the id(remember? we have assigned unique id to each buttons)
        if (button.getId() == R.id.button_submit) {
            // if the button is "Submit" button display in the TextView.
            // Here we have introduced displayResult() method to show the result.
            // This makes code more readable
            displayResult();

            // We had hidden the Button for going to next activity by setting the visibility to GONE
            // in the xml layout(activity_first.xml).
            // Lets make that Button Visible when we click submit.
            buttonNextActivity.setVisibility(View.VISIBLE);
        } else if (button.getId() == R.id.button_next_activity) {
            // if the button is "Go to next Activity" we are opening the "SecondActivity"
            // Lets make a method which will handle this work as above.
            // Of course this will make code easy to understand
            goToNextActivity();
        }
    }

    /**
     * A method to display the result in the displayTextView when the "Submit" button is clicked.
     */
    private void displayResult() {
        // Lets get what the user have entered in the EditText fields.
        // First get the name
        String name = editTextName.getText().toString();
        // Then the address
        String address = editTextAddress.getText().toString();
        // And next the phone number
        String phone = editTextPhone.getText().toString();

        // Lets make the message to display more meaningful by adding some stuffs.
        // This is called string concatenation i.e joining many strings to make a single string.
        String messageToShow = "Hello " + name + ". So you live in " + address +
                " and I can contact you in " + phone;

        // Then set the result to show in the TextView
        textViewDisplay.setText(messageToShow);
    }

    /**
     * A method to go from FirstActivity to SecondActivity when the "
     */
    private void goToNextActivity() {
        // Intent is used as a mediator(lets say) when we want to go from one activity to another
        // (in simple one page to another in general terms).
        // The first parameter is the current Activity(i.e FirstActivity) and second one is
        // the activity we want to open(i.e SecondActivity)
        // Notice FirstActivity.this and SecondActivity.class, this should be remembered.
        Intent intent = new Intent(FirstActivity.this, SecondActivity.class);
        // Intent is the mediator right? So we can use it to send some data from one activity to
        // another. Lets send the "name" entered by user to another activity
        // We have to use the form (key, value) so that the receiving activity knows how to get the
        // sent data(name).
        intent.putExtra("key_name", editTextName.getText().toString());
        // now just call startActivity with intent as parameter to start the next activity.
        startActivity(intent);
    }

    @Override
    public boolean onLongClick(View v) {
        // Toast is short temporary message shown to the user.
        // You might have noticed it while sending sms. you get short temporary message like
        // "Delivered", "Sent", etc.
        Toast.makeText(this, "Hello " + editTextName.getText().toString(), Toast.LENGTH_SHORT).show();
        // We must return true when we have handled the LongClick. It is compulsory.
        // Notice the return type(boolean) in the method.
        // Also notice onClick had void return type so we did not have to return anything
        return true;
    }
}
