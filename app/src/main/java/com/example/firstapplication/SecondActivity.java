package com.example.firstapplication;

import android.app.Activity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * This the second activity.
 */
public class SecondActivity extends Activity {

    private TextView textViewMessageFromFirst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        // First Activity has sent some data
        // lets get it
        String messageFromFirstActivity = getIntent() // get the Intent (the mediator)
                .getStringExtra("key_name");// The name sent from first activity
        // remember the key used while sending.
        // same key has to be used.

        //lets make the message more interesting. Remember concatenation?
        messageFromFirstActivity = "Hello " + messageFromFirstActivity + ". So you have come from First Activity";

        // Lets initialize the TextView so that we can show the name in it.
        textViewMessageFromFirst = (TextView) findViewById(R.id.textview_message_from_first_activity);
        // And set the message in it
        textViewMessageFromFirst.setText(messageFromFirstActivity);
    }
}
