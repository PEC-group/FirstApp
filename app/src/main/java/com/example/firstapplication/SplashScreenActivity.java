package com.example.firstapplication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by rabin on 5/20/2015.
 */
public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splashscreen);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startFirstActivity();
            }
        }, 2000);
    }

    private void startFirstActivity() {
        Intent intent = new Intent(SplashScreenActivity.this, FirstActivity.class);
        startActivity(intent);
        finish();
    }
}
